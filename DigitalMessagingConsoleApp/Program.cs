﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalMessagingConsoleApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length > 0)
            {
                foreach (string str in args)
                {
                    if (str.ToLower().Contains("admin"))
                    {
                        Application.Run(new Form1());
                    }else
                    {
                        RunEOTSetup();
                    }
                }
            }else
            {
                RunEOTSetup();
            }
            
        }

        static void RunEOTSetup()
        {
            MessageBox.Show("Run Scheduled Task");
        }
    }
}
